package TestCases;

import Base.TestBase;
import Pages.HomePage;
import Pages.LoginPage;
import Pages.ProductsGridPage;
import Utils.Utils;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class HomePageTest extends TestBase {

    HomePage homePage;
    ProductsGridPage productsgridpage;

    HomePageTest() {
        super();
    }

    @BeforeMethod
    public void setup() {
        launchBrowser();
        LoginPage loginPage = new LoginPage();
        homePage = loginPage.login(prop.getProperty("username"),prop.getProperty("password"));

    }

    @Test(priority = 3)
    public void performsearchTest() {
        productsgridpage = homePage.performsearch();
    }

    @Test(priority = 2)
    public void homePageLogoTest() {
        boolean logo = homePage.homePageLogo();
        Assert.assertTrue(logo);
    }

    @Test(priority = 1)
    public void validateTitleTest() {
        String title = homePage.homePageTile();
        Assert.assertEquals(title, "noon: Online Shopping UAE | Mobiles, Beauty, Appliances, Fashion");

    }
    @AfterMethod
    public void tearDown(){
        System.out.println("dummy code");
        driver.quit();
    }
}
