package TestCases;

import Base.TestBase;
import Pages.HomePage;
import Pages.LoginPage;
//import org.apache.log4j.Priority;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class LoginPageTest extends TestBase {
LoginPage loginpage;
HomePage homepage;
    public LoginPageTest(){
        super();
    }
    @BeforeMethod
    public void setup(){
        launchBrowser();
        loginpage= new LoginPage();
    }
    @Test(priority=1)
    public void validateTitleTest(){
        String title= loginpage.validateTitle();
        Assert.assertEquals(title,"noon: Online Shopping UAE | Mobiles, Beauty, Appliances, Fashion");
    }
    @Test(priority=2)
    public void siteLogoTest(){
       boolean logo=  loginpage.siteLogo();
       Assert.assertTrue(logo);
    }
    @Test(priority=3)
    public void loginTest(){
        homepage= loginpage.login(prop.getProperty("username"),prop.getProperty("password"));

    }

    @AfterMethod
    public void tearDown(){
        System.out.println("dummy code");
        driver.quit();
    }


}
