package Pages;

import Base.TestBase;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import javax.swing.*;

public class HomePage extends TestBase {

    public void HomePage(){
        PageFactory.initElements(driver,this);
    }
    @FindBy(xpath = "//img[@alt='noon']")
    WebElement logo;
    @FindBy(xpath = "//input[@id='searchBar']")
    WebElement searchbar;
    @FindBy(xpath = "//span[@data-qa='dd_user_menu']")
    WebElement myAccountText;


    public String homePageTile(){
    return driver.getTitle();
    }
    public boolean homePageLogo(){
        return logo.isDisplayed();
    }
    public boolean myAccountText(){
        return myAccountText.isDisplayed();
    }
    public ProductsGridPage performsearch(){
        searchbar.click();
        searchbar.sendKeys(prop.getProperty("productname"));
        Actions action = new Actions(driver);
        action.sendKeys(Keys.ENTER);
        return  new ProductsGridPage();
    }
}
