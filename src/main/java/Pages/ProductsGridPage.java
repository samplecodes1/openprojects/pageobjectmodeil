package Pages;

import Base.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProductsGridPage extends TestBase {
    HomePage homePage;

    @FindBy(xpath = "//span[@class='a-size-medium-plus a-color-base a-text-bold']")
    WebElement resulttext;
    @FindBy(xpath = "//span[normalize-space()='Delivery Day']")
    WebElement deliveryDayText;
    @FindBy(xpath = "//span[normalize-space()='Category']")
    WebElement categoryText;
    @FindBy(xpath = "//span[normalize-space()='Customer Review']")
    WebElement customerreviewtext;

    @FindBy(xpath = "(//div[contains(@class,'s-list-col-right')])[1]")
    WebElement displayedProduct;
    public boolean verifydeliveryDayText(){
    return deliveryDayText.isDisplayed();
    }
    public boolean verifyResulttext(){
        return resulttext.isDisplayed();
    }
    public boolean verifycategoryText(){
        return categoryText.isDisplayed();
    }
    public ProductDetailPage clickondisplayedProduct(){
        displayedProduct.click();
        return new ProductDetailPage();

    }

}
