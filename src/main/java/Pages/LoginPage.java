package Pages;

import Base.TestBase;
import Utils.Utils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends TestBase {

    public LoginPage(){

        PageFactory.initElements(driver,this);
    }
    @FindBy(xpath = "//span[@id='dd_header_signInOrUp']")
    WebElement SigninLink;
    @FindBy(xpath = "//img[@alt='noon']")
    WebElement siteLogoImage;
    @FindBy(xpath = "//button[@id='login-submit']")
    WebElement LoginButton;
    @FindBy(xpath = "//h3[normalize-space()='Sign Up']")
    WebElement signuptext;
    @FindBy(xpath = "//input[@id='emailInput']")
    WebElement emailInput;
    @FindBy(xpath = "//span[@data-qa='dd_user_menu']")
    WebElement myAccountText;
    @FindBy(xpath = "//a[normalize-space()='Forgot your password?']")
    WebElement forgotPasswordLink;
    @FindBy(xpath = "//input[@id='passwordInput']")
    WebElement passwordInput;

    public boolean siteLogo()
    {
       boolean logo=  siteLogoImage.isDisplayed();
       return logo;
    }
    public String validateTitle(){
        String title= driver.getTitle();
        return title;
    }
    public String forgotPasswordTextValidation(){
        String text = forgotPasswordLink.getText();
        return text;
    }
    public HomePage login(String un, String pw){
        SigninLink.click();
        emailInput.sendKeys(un);
        passwordInput.click();
        passwordInput.sendKeys(pw);
        LoginButton.click();

        return new HomePage();
    }

}
