package Base;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.BeforeTest;

import java.io.File;
import java.io.FileInputStream;
import java.time.Duration;
import java.util.Properties;

public class TestBase {

   public static WebDriver driver;
    public static Properties prop;


    public TestBase(){
        try{
            prop = new Properties();
            FileInputStream fis= new FileInputStream(System.getProperty("user.dir")+ "/src/main/java"
                    + "/config/config.properties");
             prop.load(fis);
            String browserName = prop.getProperty("browser");
            System.out.println(browserName);
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    public static void launchBrowser() {
        String browserName = prop.getProperty("browser");
        if (browserName.equals("chrome")) {
            WebDriverManager.chromedriver().setup();
            //  System.setProperty("webdriver.chrome.driver", "C:\\NoonV2\\chromedriver-win64\\chromedriver-win64\\chromedriver.exe");
            ChromeOptions options = new ChromeOptions();
            options.addArguments("-incognito");

            options.addArguments("--disable-popup-blocking");
            //  options.setBinary("C:\\NoonV2\\chrome-win64\\chrome-win64\\chrome.exe");
            driver = new ChromeDriver(options);
            driver.manage().window().maximize();
            driver.get(prop.getProperty("url"));
            driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
            driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(15));

        }
    }
}
